FROM openjdk:8-jdk-alpine
WORKDIR /app
COPY /proga.java /app/proga.java
RUN javac /app/proga.java
ENTRYPOINT ["java", "-cp", "/app", "proga"]